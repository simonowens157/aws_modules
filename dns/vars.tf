variable "region" {
  description = "The AWS region"
}
variable "domain" {
  description = "The domain of your website"
}
