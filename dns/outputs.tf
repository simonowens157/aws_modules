output "nameservers" {
  value = "${module.r53_mydomain.ns_servers}"
}
output "zone_id" {
  value = "${module.r53_mydomain.zone_id}"
}