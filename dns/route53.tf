terraform {
    backend "s3" {}
}
provider "aws" {
  region = "${var.region}"
}
module "r53_mydomain" {
  source  = "Aplyca/route53/aws"

  description = "DNS records zone for My Domain ${var.domain}"
  domain = "${var.domain}"
  records = {
    names = [
      "www.",
      "info.",
    ]
    types = [
      "CNAME",
      "CNAME"
    ]
    ttls = [
      "3600",
      "3600",
    ]
    values = [
      "${var.domain}",
      "${var.domain}",
    ]
  }

  alias = {
    names = [
      ""
    ]
    values = [
      "d130easdflja734js.cloudfront.net"
    ]
    zones_id = [
      "Z2FDRFHATA1ER4"
    ]
  }
}